### Multiple incoming folders

If you have a more advanced setup by using "procmail" or something
similar so that your incoming emails are automatically sorted into
separate folders, then you can inform mutt about where to look for new
mail by means of the "**mailboxes**" command. You can use "mutt -y" to
start up with the list of incoming folders. Within mutt you can toggle
the folder-browser view between regular folders and "mailboxes": enter
folder-browser with "c?", then hit "TAB" (the default for
"toggle-mailboxes" function, hit "?" again for actual key-binding). More
on this later.

You can save a lot of typing if you have many "mailboxes" entries, see
[MuttFaq/RemoteFolder](MuttFaq/RemoteFolder).
