### How to use this Guide.

It's crucial for all the documentation provided on this [MuttWiki](home) that
you read the **[manual.txt](http://www.mutt.org/doc/manual.txt)**.

This guide is not meant as replacement for the manual.txt, but as an
alternative form of presentation. You don't have to read the reference
manual.txt before reading the [MuttGuide](MuttGuide),
but you should at least look up references to variables & commands
(**"$var" & "cmd"**) when you encounter them. If you don't know which
key produces a desired function,
hit "?" to see the key-binding for the current context menu (it changes
depending on what you do).

This [MuttGuide](MuttGuide) is meant to be used from beginning to end, as a whole;
the parts are kept simple to explain the various features step by step,
and as basic as possible. Therefore not everything will be explained in
all aspects & detail in each part. You might have to read several parts
to learn everything for a certain task before you can make it work as
you like. Mutt's power lies in the combination of the various features,
and it depends on your creativity to make full use of it.

Working with email is a complex task, even though common user front ends
might make it seem simple. Well, it can be simple, if you have some
system administrator who takes care of all the stuff that happens on the
way between two end users. Even if you are not such an admin who has to
personally take care of everything in the own system, it will definitely
help analyzing issues if you take time to understand the basic mail
relationship by reading the **[MailConcept](MailConcept)**.

Should you run into trouble with your config, [DebugConfig](DebugConfig) has ways to
help you track it down and (possibly) solve it. Once you're done with
the Guide or when debugging doesn't help, have a look at the [MuttFaq](MuttFaq),
too.

Remember there are 3 levels regarding mutt usage:

1. the build (compilation),   
2. the configuration and the   
3. interaction.

On every level you have choices to make about what you want to do, so
always make up your mind, take care and don't depend on defaults: admins
and distributors tend to customize global configs. So make sure you
explicitly set what you need.

The Wiki assumes that you are familiar with editing text files and
working with your command-shell and its features, especially the meaning
of "shell environment variables" (keyword "export").

Read the man-page by executing "**man $SHELL**" in a command-line
prompt.

And, of course, you should be willing to follow any references to
external documents and read them.
(Examples are the documents of other applications used with mutt, and of
course mutt's manual.txt).

### How to help this Guide.

Always keep in mind that new users are expected to get through this all
on their own.

This is not a task for experts only! Especially **new users
themselves** are the best to tell what helped them to get started and
how to change the docs to make it easier for the next one reading it. So
add what **you** learn to share it with your fateful comrades who need
the help that you can provide.

Before you edit, read **[WikiWorker](WikiWorker)** first!

Note: you are not alone. There are many like you who need help or want
to help out. Just get started, others can and will continue your work.

Please join us!
