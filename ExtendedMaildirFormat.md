Based on the basic [MaildirFormat](MaildirFormat), Courier introduced some extensions,
namely subfolders and quotas: <http://www.courier-mta.org/maildir.html>

Subfolders are Maildirs starting with a dot in the toplevel Maildir, to
designate a subfolder being inside another subfolder, prepend that other
subfolders name to it. This leads to a layout that looks like this:

``` 
 Maildir/
         cur/
         new/
         tmp/
         .Mutt.dev/
                  cur/
                  new/
                  tmp/
         .Mutt.users/
                  cur/
                  new/
                  tmp/
         .Sent
                  cur/
                  new/
                  tmp/
```

Besides the INBOX there's a Sent folder and the (virtual) Mutt folder
which contains both the users and dev folders. Accessing this through an
IMAP server with some GUI MUA would show a tree like that:

``` 
 INBOX
 |-Mutt
 | |-dev
 | `-users
 `-Sent
```

    #!comment
    http://www.flounder.net/~mrsam/maildrop/maildirmake.html broken

Compatible implementations of
[maildirmake](http://www.flounder.net/~mrsam/maildrop/maildirmake.html)
as shipped with Courier, dovecot and maildrop for example allow for easy
creation of subfolders.

## Information about kmail

Apparently, ExtendedMaildir implementation is different for kmail
1.7.1. This information should be important if you use getmail or
fetchmail and to use several mailer. For the same tree, it's look like :

``` 
 Maildir/
         inbox/
             cur/
             new/
             tmp/
         .inbox.directory/
             Mutt/
                 cur/
                 new/
                 tmp/
             .Mutt.index
             .Mutt.index.ids
             .Mutt.directory/
                 dev/
                    cur/
                    new/
                    tmp/
                 users/
                    cur/
                    new/
                    tmp/
                 .dev.index
                 .dev.index.ids
                 .users.index
                 .users.index.ids
         sent/
             cur/
             new/
             tmp/
```

It would be useful to have a good RFC for maildir ...

## Questions

Can the mutt file browser be configured to display .Mutt.dev and
.Mutt.users as a folder named Mutt containing mailboxes named dev and
users?
