### "XYZ is not a mailbox" or "has no messages", what does this mean?

In mbox-format this means the folder you are about to access doesn't
conform to the RFC2822 specification. Either it really is not meant to
be a mailbox, or the folder has been corrupted. Typical corruption is
incorrect 1st line, which should look like this:

``` 
 From user@domain date
```

(note the missing ":" after "From") or missing empty line between header
& body (empty == not even blanks, absolutely no characters).

For maildir-format this typically results from missing required subdirs.
See [FolderFormat](FolderFormat) for how they should be setup correctly!

Some people mistakenly create folders of one format (for example by
procmail) and try to use some other format in mutt.
Example:

* bad procmail-config: creating regular flat-file mbox instead of maildir, the trailing "/" makes the difference, "man procmailrc"!!!   
* bad muttrc: specifying an explicit trailing "/"; mutt auto-detects the right format when reading a folder, it **`chokes`** on trailing "/".

So make sure you create & use the same folder format: drop the "/" in
muttrc, choose the right one for procmail!!!

In case it's a *missing* **"From "**-line, solutions to fix new mail
vary by program(s) used to get new
mail:

* **procmail**: have a look at the procmail manpage and check out the "-f" option.  
* **fetchmail**: see its manpage to create the From-line for you,
  or how to invoke your MDA (typically procmail) properly, see above.  
* **getmail**: it is likely you do not have the unixfrom variable set in your getmailrc.  
  * In ~/.getmail/getmailrc under [destination] set unixfrom to yes; e.g.,  
    ```
    [destination]
     type = MDA_external
     path = /usr/bin/procmail
     unixfrom = yes
    ```

You can *add missing* **"From "** lines of already delivered emails by
means of piping them through formail like

`cat mailbox | formail -b > new_mailbox`

Depending on how broken the mailbox is, may or may not work. In doubt
add 'From ' lines manually.

Some mailing-list archives *corrupt* the original "From " lines to make
them less useful for spammer bots. If you're lucky, then a simple
replace-function can "fix" this so you can use it with mutt again. See
"man sed",

`sed -e '/!^From / s, at ,@,' < source-file > other-file`

Sometimes people forget to specify some important [MuttGuide/Folders](MuttGuide/Folders) and
wonder why mutt complains about something not existing or working
properly of which they never heard before. Make sure you're not missing
something, too! Go back and check.

### Why are "new" flags of mbox folders wrong in folder-list view?

As indicated in the [manual.txt]([MuttWiki](home) "wikilink"), flags are
determined by comparing the timestamps of last access and modification.
This can get messed up if the folders are "touched" by other programs
than mutt, like "biff" or backup software.

There is also some issue with the "noatime" flag for mounting
filesystems (most often used on laptops). If "noatime" is activated, no
timestamp is updated for the last folder access, i.e. Mutt cannot
determine if the folder has received new mail since last visited. For
mutt before version 1.5.15, you can recompile it with the
--enable-buffy-size option to "configure"; for mutt 1.5.15 or later see
the $check\_mbox\_size option. Mutt will then use the folder size
instead of the access times. (This is only a workaround and might give
suboptimal results; another option is to use the [MaildirFormat](MaildirFormat).)

### How to make mutt open a specific folder on startup?

For casual use consider

``` 
 mutt -f path/folder
```

To do it every time rtfm

``` 
 $spoolfile
```

Note for both: **$folder** can help you shorten the paths you have to
specify, and lookout for the many other useful [folder
shortcuts]([MuttGuide/Folders](MuttGuide/Folders) "wikilink").

### How to save copies of outgoing/sent eMails?

RTFM **$copy, $record** for more. By default NONE is saved anywhere,
make sure you set them before you need them.

### How to save copies of outgoing/sent eMails to the current folder?

You need the current\_shortcut patch (see [PatchList](PatchList)). With that patch,
\`"^"\` points to the current folder. You can use that folder in
folder-hooks etc.

``` 
 folder-hook . 'set record="^"'
```

**Note:** Make sure you have at least current\_shortcut.**2**. Version 1
is
broken.

### I can't modify my inbox folder, Mutt claims it's read-only, but permissions are ok?

Often this happens due to locking problems. To manipulate folders the
access has to be exclusive to avoid corruptions by concurrent write
operations. Chances are that for the directory containing the folder
causing trouble the permissions are disallowing write access or that
your

``` 
 $mutt_dotlock
```

either doesn't point to a suitable filelock-program, or the permissons
for the lock-programm are not correct. Normally it should look like
this:

``` 
               user    group     filename
 -r-xr-sr-x    bin      mail     mutt_dotlock
 drwxrws--x    root     mail     /var/mail
```

The important bit is that mutt\_dotlock's group must match /var/mail's,
and mutt\_dotlock's sgid-bit is set. Only the admin can

``` 
 $ sudo chgrp mail mutt_dotlock; sudo chmod g+s mutt_dotlock
```

If it's not your own system, ask the sysadmins to install mutt\_dotlock
correctly.

Alternatively you can try to use [MaildirFormat](MaildirFormat) (remember to have the
[MDA as well as the MUA]([MailConcept](MailConcept) "wikilink") use the same format).
It has no locking issues, but the individual messages are organized
differently than classical
[MBOX-format]([FolderFormat](FolderFormat) "wikilink").

### How to make mutt check for new mail more often?  What's the difference between $timeout and $mail\_check?

After every keyboard input mutt updates the status of all folders. To
receive "New mail in ..." notifications even without needing to press a
key, set **$timeout** == time to wait for idle mutt (no key pressed)
before the status is updated again as if a key were pressed. To avoid
too frequent folder access (bad connections via NFS or IMAP), set
**$mail\_check** == minium time between 2 scans for new mail (external
changes to folders) in case of high keyboard activity.

`$mail_check < $timeout : scan on next update`  
`$timeout < $mail_check : update before scan`

This means **$mail\_check \< $timeout** is more useful, because by the
time mutt will update, it will also scan for external changes to
incorporate them in the update.

### How to get informed about new mail?

When new mail arrives, an automatic (no key pressed) "New mail in ..."
notification is shown at the screen bottom. This happens **only** in the
index menu. For manual checking, you can use the *buffy-list* function
which works in the pager, index and folder browser. It prints a list of
folders with new mail. However, it will display an up-to-date list only
when the index menu is focused. Additionally, you can invoke *check-new*
in the folder browser which updates the display ('N' flag for folders
with new mail) and also *buffy-list*'s folder list.

### Stop asking to "move read messages to mbox"!

RTFM "move", i.e. "**set move=no**"

### How to create new folders?

Just copy or save (see /Action) to the folder you want. If it doesn't
exist, mutt will create it for you with the [FolderFormat](FolderFormat) you specify.

### How to use the "trash" folder?

Mutt doesn't provide a "trash" system by itself. A reason might be that
people forget to purge it from time to time. Anyway, if you really want
to use such a thing, there exist [MacroSamples](MacroSamples). There is also a trash
folder patch (see [PatchList](PatchList)), here's an example setup that automatically
selects different trash and record folders depending on the current
mailbox (the ^ is from the current\_shortcut patch):

``` 
 folder-hook .         'set record="!" trash="=trash/trash"'
 folder-hook IN/.      'set record="^" trash="^.trash"'
 folder-hook \.trash$  'unset trash'
```

### How to import messages from pine, thunderbird, kmail, evilution, ...?

Mutt understands some standard [FolderFormat](FolderFormat)""s. Find out what format the
other MUA uses, and when it's compatible, just copy them over to a place
for mutt to find them (and configure it per [MuttGuide/Folders](MuttGuide/Folders)). If it's
a proprietary format, either use the MUAs own **export** function, or
search the web for external tools which can convert to a mutt-usable
format, and proceed as before with configuring mutt to find
them.

### I left off a trailing '/' in my .procmailrc, and procmail delivered many emails to a Maildir folder in MH format. How do I fix this?

Install the **nmh** program on your system, e.g. via your system's
package manager.

Then set up $HOME/.mh\_profile as follows:

``` 
 Path:                $HOME/$MAIL
 Inbox:               $HOME/$INBOX
 Unseen-Sequence:     unseen
```

where

* **`$MAIL`** is the path to a folder that can be used as the default mail folder for nmh;
  the **`Path:`** option is required by the **`nmh`** suite of programs.

* **`$INBOX`** is the path to the folder in which you'd like to view the MH emails.

* The **`Unseen-Sequence`** option will mark the messages as unread.

Change to the directory which contains the MH messages you want to be
able to view, and type the following on the command line, assuming the
**bash** shell:
```bash
     for msg in *; do cat $msg | $RCVSTOREPATH/rcvstore -unseen; done
```
where **$RCVSTORE** is the location of the **rcvstore** program (perhaps
**/usr/lib/nmh/**).

You should now be able to view the messages in Mutt. (If not, you may
need to do a **touch .mh\_sequences** in the **$INBOX** directory and/or
temporarily move the **cur**, **new** and **tmp** folders to another
location, so that Mutt regards **$INBOX** as a MH folder.) You can then
use Mutt to move the messages to one or more Maildir folders
temporarily; remove the **.mh\_sequences** file from the **$INBOX**
directory and move the **cur**, **new** and **tmp** folders back to
**$INBOX** if necessary, so that Mutt will again regard **$INBOX** as a
Maildir folder; and then use move the messages from their temporary
location back to **$INBOX**.

### What's the difference between '+' and '='?

Technically NONE, it's just for convenience not to need SHIFT to type it
depending on keyboard layout (DE, EN).
