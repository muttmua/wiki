Here go only 3rd party tools that enhance or expand mutt run-time
functionality other than those for the [QueryCommand](QueryCommand)! Note: this is just
for run-time, not compile-time extensions (see [PatchList](PatchList) for
compile-time). You can find **more tools** on some
[UserPages](UserPages).

* [mutt print](http://muttprint.sourceforge.net/) formats messages for
  printing like pine.

* [antiword](http://www.winfield.demon.nl/) M$-Office-Word to txt
  converter.

* [rtf2html](http://www.wagner.pp.ru/~vitus/software/catdoc/) RTF to
  HTML converter (which then can be piped through `lynx -stdin -dump
  -force_html`).

* [xlhtml](http://chicago.sourceforge.net/xlhtml/) M$-Excel,
  Powerpoint to HTML converter, see rtf2html.

* [abook](http://abook.sourceforge.net/) Text-based address book
  program.

* [PGP](http://www.pgpi.org/) message encryption for privacy.

* [GPG, G*nu*PG](http://www.gnupg.org/) GNU's privacy guard, a free
  (FOSS) replacement for PGP.

* [MailtoMutt](http://mailtomutt.sourceforge.net/) A Cocoa application
  which handles mail to URLs, forwarding them to mutt using
  applescript and Terminal.app.

* [post mode](http://post-mode.sourceforge.net/) An Emacs mode for
  composing email or USENET messages for an external user agent (mutt,
  slrn, etc.).

* [mairix](http://www.rpcurnow.force9.co.uk/mairix/) An excellent
  mailbox searching tool for mutt. See UserStory/SearchingMail

* [nmzmail](http://www.ecademix.com/JohannesHofmann/#nmzmail) Another
  excellent mailbox searching tool for mutt. See
  UserStory/SearchingMail

* [t-prot](http://www.escape.de/users/tolot/mutt/) A program to clean
  up various email problems (long signatures, incorrect quoting,
  etc.).

* [isync](http://isync.sourceforge.net/) may be better than Mutt with
  IMAP header caching.

* [mswatch](http://mswatch.sourceforge.net/) use with isync to sync
  local and server IMAP stores to track changes as they happen.

* [offlineimap](http://offlineimap.org/) alternative to isync.

* [muttzilla](http://sourceforge.net/projects/muttzilla/) Make mutt
  your mail reader in Netscape or Mozilla.

* [goobook](https://pypi.python.org/pypi/goobook/) "Search your google
  contacts from the command-line or mutt."

* [win32 port](http://www.oocities.org/win32mutt/win32.html) run mutt
  on windows.

* [mutt-vid](https://gitlab.com/protist/mutt-vid) sets the sender
  account to that used last for the addressee.

* [mutt-alias](https://github.com/Konfekt/mutt-alias) adds aliases for
  all e-mail addresses found in a maildir such as the Inbox or Sent
  folder.

* [mutt-trim](https://github.com/Konfekt/mutt-trim) unclutters and
  normalizes quoted text in an e-mail.

* [vcalendar-filter](https://github.com/terabyte/mutt-filters/blob/master/vcalendar-filter)
  views calendar files (ics).

* [mutt2khal](https://github.com/pimutils/khal/blob/master/misc/mutt2khal)
  passes calendar items to the command-line calendar Khal.

* [vim-mutt-aliases](https://github.com/Konfekt/vim-mutt-aliases/)
  completes mail aliases in Vim.

* [vim-mailquery](https://github.com/Konfekt/vim-mailquery) completes
  mail addresses in your inbox in Vim.

* [lesspipe.sh](https://github.com/wofr06/lesspipe) a universal terminal reader for any
  file format. To use it in `mutt`, add
  ```
  autoview text/* application/* image/* audio/*
  ```
  to your `.muttrc` and
  ```
  text/*;                           LESSQUIET=1 lesspipe.sh '%s'; copiousoutput
  application/*;                    LESSQUIET=1 lesspipe.sh '%s'; copiousoutput
  image/*;                          LESSQUIET=1 lesspipe.sh '%s'; copiousoutput
  audio/*;                          LESSQUIET=1 lesspipe.sh '%s'; copiousoutput
  ```
  to the file given by the value of `mailcap_path`.
