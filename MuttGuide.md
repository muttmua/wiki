This is a generic in-depth **functionality** oriented guide to what mutt
can do for you, from first steps, getting things running, and high-end
fine tuning as seen in various configs from [ConfigList](ConfigList) & [ConfigTricks](ConfigTricks).  

The [UseCases](UseCases) show how to reach a specific goal and so use a different
approach.

Please look at the current documentation to see if your requests for
specific features in Form, Structure, Style, Content and Topics aren't
already provided.

If you can't find it you can provide it yourself or make a request for
it by putting it at the bottom in the **to-do:**
list:  


## Basics to get going

* [MuttGuide/Introduction](MuttGuide/Introduction): how to use this guide.  
* [MailConcept](MailConcept): how mail flow between computers and your mailbox.  
* [MuttGuide/Setup](MuttGuide/Setup): compile-time, install, extra packages.  
* [MuttGuide/Syntax](MuttGuide/Syntax): the run-time config file.  
* [MuttGuide/Folders](MuttGuide/Folders): management, mbox, maildir, subfolders, shortcuts.  
* [MuttGuide/Actions](MuttGuide/Actions) (views, menus): modes, save, reply, forward, change folder, macros.
* [MuttGuide/Send](MuttGuide/Send): concept, compose, encoding.  
 * [MuttGuide/Headers](MuttGuide/Headers): change From-ID, envelopes, alternates.  
 * [MuttGuide/Aliases](MuttGuide/Aliases): address management, external database.
* [MuttGuide/Receive](MuttGuide/Receive): concept, spoolfile, mailboxes, notifications.  
 * [MuttGuide/Filesystem](MuttGuide/Filesystem) local, NFS, permission, locking, and mutt_dotlock.  
 * [MuttGuide/UsePOP](MuttGuide/UsePOP): getting + processing, filter: fetchmail, procmail  
 * [MuttGuide/UseIMAP](MuttGuide/UseIMAP): remote folders.

## Advanced stuff

* [MuttFaq/Charset](MuttFaq/Charset): what and why things can go wrong.  
* Encryption: why, why not, security in stored files.  
 * [MuttGuide/UsePGP](MuttGuide/UsePGP)  
 * [MuttGuide/UseGPG](MuttGuide/UseGPG)  
 * [MuttGuide/UseSMIME](MuttGuide/UseSMIME)

* Dynamic & complex configuration for special tasks.  
 * [MuttGuide/Macros](MuttGuide/Macros): save yourself from typing the same repetitive stuff.  
 * [MuttGuide/Hooks](MuttGuide/Hooks): automatic actions.
 * [MuttGuide/Archiving](MuttGuide/Archiving)  
 * [MuttGuide/MultiAccounts](MuttGuide/MultiAccounts): change profiles/ roles/ identities/ accounts/ From:-addresses.

* Misc:  
 * [ConfigTricks](ConfigTricks): general tips.  
 * [MuttGuide/XFace](MuttGuide/XFace): inclusion and display.  
 * [MuttGuide/Groups](MuttGuide/Groups)  
 * [MuttGuide/Caching](MuttGuide/Caching): Cache message headers (POP, IMAP, MH, Maildir) and bodies (POP, IMAP).

-----

to-do:

-----

If this guide doesn't answer your questions please check the [UseCases](UseCases).
